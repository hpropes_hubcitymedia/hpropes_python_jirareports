#!/usr/bin/python
# coding=utf-8

__author__ = 'hpropes'

import xml.etree.ElementTree as ET
import re
import time
import bs4
import sys
import datetime
from bs4 import BeautifulSoup
from xml import etree
import datetime
from lxml import objectify
from reportlab.lib import utils
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch, mm, cm
from reportlab.pdfgen import canvas
from reportlab.platypus import Paragraph, Image, SimpleDocTemplate, Frame, BaseDocTemplate, PageTemplate, Spacer
from reportlab.lib.enums import TA_JUSTIFY

########################################################################
class PDFOrder(object):
    """"""

    #----------------------------------------------------------------------
    def __init__(self, xml_file, pdf_file, image_file):
        """Constructor"""
        self.xml_file = xml_file
        self.pdf_file = pdf_file
        self.image_file = image_file


    #----------------------------------------------------------------------
    def coord(self, x, y, unit=1):
        """
        # http://stackoverflow.com/questions/4726011/wrap-text-in-a-table-reportlab
        Helper class to help position flowables in Canvas objects
        """
        x, y = x * unit, self.height - y * unit
        return x, y

    #----------------------------------------------------------------------

    def get_image(path, width=1*cm):
        img = utils.ImageReader(path)
        iw, ih = img.getSize()
        aspect = ih / float(iw)
        newwidth = width
        height=(newwidth * aspect)
        return Image(path, newwidth, height)

    #----------------------------------------------------------------------


    def smart_truncate1(self, text, max_length=1200, suffix='...'):
        """Returns a string of at most `max_length` characters, cutting
        only at word-boundaries. If the string was truncated, `suffix`
        will be appended.
        """

        if (len(text) > max_length):
            if text[max_length].isspace():
                newtext = text[0:max_length] + suffix
            else:
                newtext = text[0:max_length].rsplit(None, 1)[0] + suffix

            #newtext = (text[:max_length] + suffix)
            return newtext
        else:
            return text



    #----------------------------------------------------------------------

    def footerandheader(self, canvas, doc):

        pagewidth, self.height = letter
        styles = getSampleStyleSheet()
        styleN = styles['Normal']
        canvas.saveState()
        #draw the footer
        P = Paragraph("This is a FOOTER.  It goes on every page.  " * 1,
                      styleN)
        P.wrapOn(canvas, pagewidth, self.height)
        P.drawOn(canvas, *self.coord(18, 270, mm))

        #draw the header
        formatted_time = time.ctime()
        P = Paragraph(formatted_time * 1,
                      styleN)

        P.wrapOn(canvas, pagewidth, self.height)
        P.drawOn(canvas, *self.coord(18, 10, mm))

        im = Image(self.image_file)
        im._restrictSize(2 * inch, 3 * inch)
        im.wrapOn(canvas, pagewidth, self.height)
        im.drawOn(canvas, *self.coord(140, 12, mm))

        canvas.restoreState()

    #----------------------------------------------------------------------

    def createPDFNew(self):
        """
        Create a PDF based on the XML data
        """

        doc = BaseDocTemplate(self.pdf_file, pagesize=letter)
        frame = Frame(doc.leftMargin, doc.bottomMargin, doc.width, doc.height,
               id='normal')
        template = PageTemplate(id='test', frames=frame, onPage=self.footerandheader)
        doc.addPageTemplates([template])

        Story = []
        tree = ET.parse(self.xml_file)
        channel = tree.find("channel")

        #Story.append(im)
        #p.wrapOn(self.canvas, width, self.height)
        #p.drawOn(self.canvas, *self.coord(18, 30, mm))
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))

        header = """ <font size="12">
        <b>%s Report</b>
        </font>
        """ % (channel.find("title").text)
        Story.append(Paragraph(header, styles["Normal"]))
        Story.append(Spacer(1, 12))

        for item in channel.findall('item'):

            link = item.find('link').text

            line1 = """ <font size="10"> <b><a href=\"%s\">%s</a></b> </font> """ % (link, item.find('title').text)
            Story.append(Paragraph(line1, styles["Normal"]))

            line2 = """ <font size="10"> <b>%s</b> - %s </font> """ % ("Created:",item.find('created').text)
            Story.append(Paragraph(line2, styles["Normal"]))

            if item.find('description').text:
                description = BeautifulSoup(item.find('description').text)
                cleantext = description.text

            #make this smarter so it does not truncate a word
            description_new = self.smart_truncate1(cleantext)

            print("description",description_new)

            line7 = """ <font size="10"> <b>%s</b> - %s </font> """ % ("Description:", description_new)
            Story.append(Paragraph(line7, styles["Normal"]))

            line4 = """ <font size="10"> <b>%s</b> - %s </font> """ % ("Status:",item.find('status').text)
            Story.append(Paragraph(line4, styles["Normal"]))

            if(item.find('resolved')):
                line3 = """ <font size="10"> <b>%s</b> - %s </font> """ % ("Resolved:",item.find('resolved').text)
                Story.append(Paragraph(line3, styles["Normal"]))

            commentsblock = item.find("comments")
            if(commentsblock):
                for comment in commentsblock.findall("comment[last()]"):
                    #comment = commentsblock.findall("comment[-1]")
                    comment_new = BeautifulSoup(comment.text)
                    comment_created = comment.get("created")
                    #print("Number of comments {0}".format(str(len(comments))))
                    #comment = comments[-1]
                    cline1 = """ <font size="10"> <b>%s</b>%s <b>[%s]</b></font> """ % ("Latest Comment: ",comment_new.text,comment_created)
                    Story.append(Paragraph(cline1, styles["Normal"]))

            Story.append(Spacer(1, 12))

        doc.build(Story)


    def getXMLObject(self):
        """
        Open the XML document and return an lxml XML document
        """

        with open(self.xml_file) as f:
            xml = f.read()
        return objectify.fromstring(xml)


    #----------------------------------------------------------------------
    def savePDF(self):
        """
        Save the PDF to disk
        """

        self.canvas.save()

#----------------------------------------------------------------------

if __name__ == "__main__":
    # Input File
    XML_FILE = sys.argv[1]
    #xml = "xml/jira.xml"
    #pdf = 'pdf/jirareport-%s.pdf'%datetime.now().strftime('%Y-%m-%d:%H:%M:%S')
    pdf = 'pdf/jirareport-%s.pdf'%datetime.datetime.now().strftime('%Y-%m-%d')
    #'/path/to/output/myfile-%s.txt'%datetime.now().strftime('%Y-%m-%d:%H:%M:%S')
    image = "image/logo.png"
    doc = PDFOrder(XML_FILE, pdf, image)
    doc.createPDFNew()
    #doc.savePDF()
